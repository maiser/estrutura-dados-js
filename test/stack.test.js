let Stack = require('../class/Stack.class.js');

let expect = require('expect.js');
describe('Stack', function() {
    let s = new Stack();
    it('#push(element)', function() {
        s.push(1);
        s.push(2);
        s.push(3);
        expect(s.getStack()).to.eql([ 1, 2, 3 ]);
    });
    it('#pop()', function() {
        expect(s.pop()).to.be(3);
        expect(s.pop()).to.be(2);
        expect(s.pop()).to.be(1);
        expect(s.pop()).to.be(undefined);
    });
    it('#isEmpty()', function() {
        expect(s.isEmpty()).to.ok();
    });
    it('#clear()', function() {
        s.push(1);
        s.push(2);
        s.clear();
        expect(s.isEmpty()).to.ok();
    });
    it('#peek()', function() {
        s.push(1);
        s.push(2);
        expect(s.peek()).to.be(2);
        s.pop();
        expect(s.peek()).to.be(1);
        s.pop();
        expect(s.peek()).to.be(undefined);
    });
    it('#length()', function() {
        s.clear();
        expect(s.length()).to.be(0);
        s.push({"obj": "test"});
        expect(s.length()).to.be(1);
        s.peek();
        expect(s.length()).to.be(1);
        s.pop();
        expect(s.length()).to.be(0);
        
    });
    it('#toString()', function() {
        s.push(1);
        s.push(2);
        expect(s.toString()).to.be('2\n1\n');
    });
    it('#toString(inOrder=false)', function() {
        expect(s.toString(false)).to.be('1\n2\n');
    });
   
});
