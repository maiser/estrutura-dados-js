let PQueue = require('../class/PriorityQueue.class.js');
let expect = require('expect.js');

describe('PriorityQueue', function() {
    let pq = new PQueue();
    it('#enqueue(element, priority)', function() {
        pq.enqueue({obj: 'test'},15);
        pq.enqueue(3,10);
        pq.enqueue(1,3);
        pq.enqueue(2,5);
        expect(pq.toArray()).to.eql([ 1, 2, 3, {obj: 'test'} ]);
    });
    it('#dequeue()', function() {
        expect(pq.dequeue()).to.be(1);
        expect(pq.dequeue()).to.be(2);
        expect(pq.dequeue()).to.be(3);
        expect(pq.dequeue()).to.eql({obj: 'test'});
        expect(pq.dequeue()).to.be(undefined);
    });
    it('#isEmpty()', function() {
        expect(pq.isEmpty()).to.ok();
    });
    it('#clear()', function() {
        pq.enqueue(1,0);
        pq.enqueue(2,1);
        pq.enqueue(3,2);
        pq.clear();
        expect(pq.isEmpty()).to.ok();
    });
    it('#front()', function() {
        pq.enqueue(1,0);
        pq.enqueue(2,1);
        pq.enqueue(3,5);
        expect(pq.front()).to.be(1);
        pq.dequeue();
        expect(pq.front()).to.be(2);
        pq.dequeue();
        expect(pq.front()).to.be(3);
        pq.dequeue();
        expect(pq.front()).to.be(undefined);
    });
    it('#back()', function() {
        pq.enqueue(3,5);
        pq.enqueue(1,1);
        pq.enqueue(2,3);
        expect(pq.back()).to.be(3);
        pq.dequeue() // out 1 -> 2 3
        expect(pq.back()).to.be(3);
        pq.enqueue(4,0); // in 4 -> 4 2 3 
        expect(pq.back()).to.be(3);
        pq.dequeue() // out 4 -> 2 3
        expect(pq.back()).to.be(3);
        pq.dequeue() // out 2 -> 3
        expect(pq.back()).to.be(3);
        pq.dequeue() // out 3
        expect(pq.back()).to.be(undefined);
    });
    
    it('#length()', function() {
        pq.clear();
        expect(pq.length()).to.be(0);
        pq.enqueue({"obj": "test"});
        expect(pq.length()).to.be(1);
        pq.back();
        expect(pq.length()).to.be(1);
        pq.dequeue();
        expect(pq.length()).to.be(0);
        pq.dequeue();
        expect(pq.length()).to.be(0);
    });
    it('#toString()', function() {
        pq.enqueue(5,20);
        pq.enqueue(3,8);
        pq.enqueue(4,14);
        pq.enqueue(1,3);
        expect(pq.toString()).to.be('| 1 | 3 | 4 | 5 | ');
    });
    it('#toString(inOrder=false)', function() {
        expect(pq.toString(false)).to.be('| 5 | 4 | 3 | 1 | ');
    });
});
