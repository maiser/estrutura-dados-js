let Queue = require('../class/Queue.class.js');
let expect = require('expect.js');

describe('Queue', function() {
    let q = new Queue();
    it('#enqueue(element)', function() {
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        expect(q.getQueue()).to.eql([ 1, 2, 3 ]);
    });
    it('#dequeue()', function() {
        expect(q.dequeue()).to.be(1);
        expect(q.dequeue()).to.be(2);
        expect(q.dequeue()).to.be(3);
        expect(q.dequeue()).to.be(undefined);
    });
    it('#isEmpty()', function() {
        expect(q.isEmpty()).to.ok();
    });
    it('#clear()', function() {
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        q.clear();
        expect(q.isEmpty()).to.ok();
    });
    it('#front()', function() {
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        expect(q.front()).to.be(1);
        q.dequeue()
        expect(q.front()).to.be(2);
        q.dequeue()
        expect(q.front()).to.be(3);
        q.dequeue()
        expect(q.front()).to.be(undefined);
    });
    it('#back()', function() {
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        expect(q.back()).to.be(3);
        q.dequeue() // out 1 -> 2 3
        expect(q.back()).to.be(3);
        q.enqueue(4); // in 4 -> 2 3 4
        expect(q.back()).to.be(4);
        q.dequeue() // out 2 -> 3 4
        expect(q.back()).to.be(4);
        q.dequeue() // out 3 -> 4
        expect(q.back()).to.be(4);
        q.dequeue() // out 4
        expect(q.back()).to.be(undefined);
    });
    it('#length()', function() {
        q.clear();
        expect(q.length()).to.be(0);
        q.enqueue({"obj": "test"});
        expect(q.length()).to.be(1);
        q.back();
        expect(q.length()).to.be(1);
        q.dequeue();
        expect(q.length()).to.be(0);
        q.dequeue();
        expect(q.length()).to.be(0);
    });
    it('#toString()', function() {
        q.enqueue(1);
        q.enqueue(2);
        expect(q.toString()).to.be('| 1 | 2 | ');
    });
    it('#toString(inOrder=false)', function() {
        q.enqueue(3);
        q.enqueue(4);
        expect(q.toString(false)).to.be('| 4 | 3 | 2 | 1 | ');
    });

    
});
