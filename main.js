let Graph = require('./class/Graph.class.js');

let G = new Graph(8);

G.addEdge(1,2);
G.addEdge(1,3);
G.addEdge(1,4);
G.addEdge(2,3);
G.addEdge(3,4);
G.addEdge(4,5);
G.addEdge(5,6);
G.addEdge(6,7);
G.addEdge(7,8);
G.printEdges();
G.showGraph();


//G.printVisitedOrder(G.DFS(5));
G.printVisitedOrder(G.DFSforAll(4));




