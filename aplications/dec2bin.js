let Stack = require('../class/Stack.class.js');

//convert a decimal number to binary (base 2)
function dec2bin(decNumber){
    let dn = parseInt(decNumber);
    let bin = new Stack();
    while(dn != 0){
        resto = dn % 2;
        dn = Math.floor(dn / 2);
        bin.push(resto);
    }
    console.log(bin.toString());
}

dec2bin(100);
