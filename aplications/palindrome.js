let Queue = require('../class/Queue.class.js');

//check if a word is a palindrome
function isPalindrome(word){
    let queue = new Queue();
    for(let i=0; i < word.length; i++){
        queue.enqueue(word[i]);
    }
    let isPalindrome = true;
    while(queue.length()){
        if(queue.dequeue() != word[queue.length()]){
            isPalindrome = false;
            break;
        }
    }
    if(isPalindrome){
        console.log("The word '", word, "' is a palindrome");
    }else{
        console.log("The word '", word, "' is not a palindrome");
    }
}

isPalindrome("racecar");
isPalindrome("aba");
isPalindrome("testset");
isPalindrome("banana");