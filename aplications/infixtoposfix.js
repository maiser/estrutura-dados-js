let Stack = require('../class/Stack.class.js');

// check if element is a aritimethic operator
function isOperator(elem){
    if(elem == '+' || elem == '-' || elem == '*' || elem == '\\' || elem == '('){
        return true;
    }
}
//simple converter infix expression to a postfix expression (without operator precedence)
function infixToPosfix(infixExpression){
    let operatorStack = new Stack();
    let posfixExpression = "";

    //removing white spaces
    infixExpression = infixExpression.split(' ').join('');

    for(let i=0; i< infixExpression.length; i++){

        let elem = infixExpression[i];

        if(elem == ')'){
            let top = operatorStack.peek();
            while(!operatorStack.isEmpty() && top != '(' ){
                posfixExpression += operatorStack.pop();
                top = operatorStack.peek();
            }
            //pop the parenthesis (
            operatorStack.pop();
            
        }else{
            if(isOperator(elem)){
                operatorStack.push(elem);
            }else{
                posfixExpression += elem;
            }
        }
    }
    while(!operatorStack.isEmpty()){
        posfixExpression += operatorStack.pop();
    }
    console.log(posfixExpression);
}


infixToPosfix("((A + B) * (C + D)) / (E - F)");

