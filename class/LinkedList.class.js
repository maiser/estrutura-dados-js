'use strict';
let BaseClass = require('./BaseClass.class.js');

class Node{
    constructor(elem){
        this.element = elem;
        this.next = null;
    }
}
//LISTAS LIGADAS (Linked Lists)
class LinkedList extends BaseClass{

    constructor(verbose = false){
        super();
        this.head = null;
        this.length = 0;
        this.v = verbose;
    }
    // add a new item to the end of the list.
    append(element){

        let node = new Node(element);

        if(this.head === null){
            this.head = node;
        }else{
            let current = this.head;
            while(current.next !== null){
                current = current.next;
            }
            current.next = node;
        }
        this.length++;

        if(this.v){
            console.log('Element ', element, ' inserted  in the list');
        }
    }
    // inserts a new item at a specified position in the list.
    insert(position, element){
        let node = new Node(element);
        let current = this.head;
        let previous;
        if(position >= 0 && position <= this.length){

            if(position === 0){
                node.next = this.head;
                this.head = node;
            }else{
                for(let i = 0; i <= position; i++){
                    previous = current;
                    current = current.next;
                }
                previous.next = node;
                node.next = current;
            }
            this.length++;
            if(this.v){
                console.log('Element ', element, ' inserted in position ' + position);
            }
            return true;
        }else{
            return false;
        }
    }
    // removes an item from a specified position in the list.
    removeAt(position){
        let previous, current = this.head;
        if(position >= 0 && position < this.length){

            if(position === 0){
                this.head = current.next;
            }else{
                for(let i = 0; i < position; i++){
                    previous = current;
                    current = current.next;
                }
                previous.next = current.next;
            }
            this.length--;
            if(this.v){
                console.log('Element ', current.element, 'is removed from position ' + position );
            }
            return current.element;
        }else{
            return null;
        }

    }
    // removes an item from the list.
    remove(element){
        let index = this.indexOf(element);
        let relem = this.removeAt(index);
        if(this.v){
            if(relem !== null){
                console.log('Element ', element, ' founded and removed');
            }else{
                console.log('Element ', element, ' not found'); 
            }
        }
        return relem; 
    }
    //returns the index of the element in the list. If the element is not in the list, it returns -1.
    indexOf(element){
        let current = this.head;
        let index = 0;
        while(current !== null){
            if(current.element === element){
                if(this.v){
                    console.log('Element', element, ' found in index ' + index);
                }
                return index;
            }
            current = current.next
            index++;
        }
        if(this.v){
            console.log('Element not found in indexOf()!')
        }
    }
    // returns true if the linked list does not contain any elements and false if the size of the linked list is bigger than 0.
    isEmpty() {
        return this.length === 0;
    }
    // returns the number of elements the linked list contains. It is similar to the length property of the array.
    size() {
        return this.length;
    }
    /*
    toString = function(){};
    */
    print(){
        let current = this.head;
        while(current.next !== null){
            console.log(current.element);
            current = current.next;
        }
        console.log(current.element);
    }
}

module.exports = LinkedList;