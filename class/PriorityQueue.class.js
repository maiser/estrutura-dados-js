'use strict';
let BaseClass = require('./BaseClass.class.js');
//FILA COM PRIORIDADES
class PriorityQueue extends BaseClass{
    constructor(verbose = false){
        super();
        this.items = new Array();
        this.size = this.items.length;
        this.v = verbose;
    }
    //enfilera o elemento pela prioridade. Quanto menor o número de prioridade mais na frente da fila o elemento estara
    enqueue(element, priority=0){
        function Element(element, priority){
            this.element = element;
            this.priority = priority;
        }
        let elem = new Element(element, priority);
        let added = false;
        let pos;
        for(pos=0; pos < this.items.length; pos++){
            if(elem.priority < this.items[pos].priority){
                this.items.splice(pos, 0, elem);
                this.size++;
                added = true;
                break;
            }
        }
        if(!added){
            this.items.push(elem);
            this.size++;
        }
        if(this.v){
            console.log(element + ' is enqueued with priority ' + priority + ' on position ' + pos);
        }
    }
    dequeue(){
        let element = this.items.shift();
        if(!this.isEmpty()){
            this.size--;
            if(this.v){
                console.log(element.element + ' is dequeued');
            }
        }else{
            if(this.v){
                console.log('Do not dequeue element. The Queue is empty!');
            }
        } 
        if(element === undefined){
            return element;
        }
        return element.element;   
    }
    length(){
        return this.size;
    }
    //return the first element from the queue
    front(){
        if(this.isEmpty()){
            return undefined;
        }
        return this.items[0].element;
    }
    //return the last element from the queue
    back(){
        if(this.isEmpty()){
            return undefined;
        }
        return this.items[this.size-1].element;
    }
    isEmpty(){
        return this.size == 0;
    }
    clear(){
        this.items = [];
        this.size = 0;
        if(this.v){
            console.log('The PriorityQueue is cleared');
        }
    }
    toArray(){
        let lastIndex = this.size -1;
        let returnArray = []
        for(let i=0; i <= lastIndex; i++){
            returnArray[i] = this.items[i].element;
         }
         return returnArray;
    }
    toString(inOrder = true){
        let string = '| ';
        let lastIndex = this.size -1;
        if(!inOrder){ //reversed (last to first)
            while(lastIndex >= 0){
                string += this.items[lastIndex--].element.toString() + ' | ';
            }
        }else{
            for(let i=0; i <= lastIndex; i++){
               string += this.items[i].element.toString() + ' | ';
            }
        }
        return string;
    }
}

module.exports = PriorityQueue;

