'use strict';
let BaseClass = require('./BaseClass.class.js');
//FILA
class Queue extends BaseClass{
    constructor(verbose = false){
        super();
        this.items = new Array();
        this.size = this.items.length;
        this.v = verbose;
    }
    // enqueue the element
    enqueue(element){
        this.items.push(element);
        this.size++;
        if(this.v){
            console.log(element + ' is enqueued');
        }
    }
    // dequeue the element from the queue
    dequeue(){
        let element = this.items.shift();
        if(!this.isEmpty()){
            this.size--;
            if(this.v){
                console.log(element + ' is dequeued');
            }
        }else{
            if(this.v){
                console.log('Do not dequeue element. The Queue is empty!');
            }
        }
        return element;   
    }
    // get the size of the queue
    length(){
        return this.size;
    }
    //return the first element from the queue
    front(){
        return this.items[0];
    }
    //return the last element from the queue
    back(){
        return this.items[this.size-1];
    }
    // check if the queue is empty
    isEmpty(){
        return this.size == 0;
    }
    // clear the queue
    clear(){
        this.items = [];
        this.size = 0;
        if(this.v){
            console.log('The Queue is cleared');
        }
    }
    // return the queue array
    getQueue(){
        return this.items;
    }
    // convert to string the elements in the queue
    toString(inOrder = true){
        let string = '| ';
        let lastIndex = this.size -1;
        if(!inOrder){ //reversed (last to first)
            while(lastIndex >= 0){
                string += this.items[lastIndex--].toString() + ' | ';
            }
        }else{
            for(let i=0; i <= lastIndex; i++){
               string += this.items[i].toString() + ' | ';
            }
        }
        if(this.v){
            console.log(string);
        }
        return string;
    }
}

module.exports = Queue;

