'use strict';

const COLORS = {
    W: "White",
    G: "Green",
    B: "Blue"
}

class Vertex{
    constructor(label, content=""){
        this.label = label;
        this.content = content;
        this.visited = false;
        this.color = COLORS.W;
        this.visited_order = undefined;
    }

    visit(){
        this.visited = true;
    }

    unvisit(){
        this.visited = false;
    }

    isVisited(){
        return this.visited;
    }

    setColor(color){
        this.color = color;
    }

    setContent(content){
        this.content = content;
    }

}

class Graph{

    constructor(v){
        this.vertices = v;
        this.edges = 0;
        this.adj = [];
        this.vertex_list = [];
        this.verbose = false;

        for (let i = 1; i <= this.vertices; ++i) {
            this.adj[i] = [];
            this.vertex_list[i] = new Vertex(i);
        }

    }

    addEdge(v, w){
        if(this.adj[v] != undefined && this.adj[w] != undefined){
            this.adj[v].push(w);
            this.adj[w].push(v);
            this.edges++;
            return true;
        }else{
            return false;
        }
    }
    
    getVertexList(){
        return this.vertex_list;
    }
    
    printEdges(){
        for(let i = 1; i < this.adj.length; ++i){
            console.log(i, this.adj[i]);
        }
    }

    printVisitedOrder(visitedOrder){
        for(let i = 0; i < visitedOrder.length; i++){
            if(visitedOrder[i] !== '#'){
                process.stdout.write(this.vertex_list[visitedOrder[i]].label + '|');
            }else{
                process.stdout.write(visitedOrder[i] + '|');
            }     
        }
    }

    DFSforAll(v){
        let visitedOrder = [];
        for(let i = v; i < this.vertex_list.length ; i++){
            if(!this.vertex_list[i].isVisited()){
                visitedOrder = visitedOrder.concat(this.DFS(i));
                visitedOrder = visitedOrder.concat(['#']);
                i = 1;
            }
        }
        return visitedOrder;
    }
    getV(visitedOrder){

    }

    //Iterative Depth-first search
    //Busca em profundidade iterativa
    DFS(v){
        let visitedOrder = [];
        let order = 0;
        visitedOrder[order] = v;
        this.vertex_list[v].visit();

        if(this.verbose)
            console.log(this.vertex_list[v].label, "is visted");

        let i = 0;
        let w = this.adj[v][i];
        let len = this.adj[v].length;
        while( i <= len){
            if(!this.vertex_list[w].isVisited()){
                ++order;
                this.vertex_list[w].visit();
                visitedOrder[order] = w;
                v = w;
                i = 0;

                if(this.verbose)
                    console.log(this.vertex_list[w].label, "is visted");
            }else{
                if(i < len){//prevents w becomes undefined
                    w = this.adj[v][i];
                }
                ++i;
            }
            len = this.adj[v].length;
        }

        if(this.verbose)
            console.log(visitedOrder);

        return visitedOrder;
    }

    //Recursive Depth-first search
    //Busca em profundidade recursiva
    rDFS(v){

        this.vertex_list[v].visit();
        console.log(this.vertex_list[v].label, "is visted");

        let i = 0;
        for(let w = this.adj[v][i]; i <= this.adj[v].length; ++i){
            if(!this.vertex_list[w].isVisited()){
                this.rDFS(w);
            }
            w = this.adj[v][i];
        }
    }
    //Recursive Breadth-first search
    // PT-BR Busca em profundidade recursiva
    rBFS(v){

        this.vertex_list[v].visit();
        console.log(v, "is visted");

        let i = 0;
        for(let w = this.adj[v][i]; i <= this.adj[v].length; ++i){
            if(!this.vertex_list[w].isVisited()){
                this.rDFS(w);
            }
            w = this.adj[v][i];
        }
    }
    showGraph(){
        for (let i = 1; i <= this.vertices; ++i) {
            process.stdout.write(i + " -> ");
            for (let j = 0; j < this.vertices; ++j) {
                if (this.adj[i][j] != undefined){
                    process.stdout.write(this.adj[i][j] + ' ');
                }
            }
            process.stdout.write("\n");
        }
    }
    
}

module.exports = Graph, Vertex;