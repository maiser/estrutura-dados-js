'use strict';
let BaseClass = require('./BaseClass.class.js');
//PILHA
class Stack extends BaseClass{
    constructor(verbose = false){
        super();
        this.items = new Array();
        this.size = this.items.length;
        this.v = verbose;
    }
    // push the element to the stack (last position of array)
    push(element){
        this.items.push(element);
        this.size++;
        if(this.v){
            console.log(element + ' is pushed');
        }
    }
    // get and remove the element from the top of stack
    pop(){
        let element = this.items.pop();
        
        if(!this.isEmpty()){
            this.size--;
            if(this.v){
                console.log(element + ' is poped');
            }
        }else{
            element = undefined;
            if(this.v){
                console.log('Do not pop element. The Stack is empty!');
            }
        }    
        return element;
    }
    // return the size of stack
    length(){
        return this.size;
    }
    // get the element from the top of the stack without remove it
    peek(){
        return this.items[this.size - 1];
    }
    // check if the stack is empty
    isEmpty(){
        return this.size == 0;
    }
    // clear the stack
    clear(){
        this.items = [];
        this.size = 0;
        if(this.v){
            console.log('The Stack is cleared');
        }
    }
    // return the stack array
    getStack(){
        return this.items;
    }
    // convert the stack to string
    toString(topToBase = true){
        let string = '';
        let topIndex = this.size -1;
        if(topToBase){
            while(topIndex >= 0){
                string += this.items[topIndex--].toString() + '\n';
            }
        }else{
            let baseIndex = 0;
            while(baseIndex <= topIndex){
               string += this.items[baseIndex++].toString() + '\n';
            }
        }
        if(this.v){
            if(topToBase){
                console.log('Printing the Stack');
            }else{
                console.log('Printing the Stack in reversed order');
            }
            console.log(string);
        }
        return string;
    }
}

//module.exports = new Stack()
module.exports = Stack;

